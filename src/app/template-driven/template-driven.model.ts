export class Employee {
  name: string = '';
  address: string = '';
  email: string = '';
  role: string = '';
  contact: number = 0;
  salary: number = 0;
  action: any = '';

  constructor(employee: any) {
    this.name = employee.Name;
    this.address = employee.Address;
    this.email = employee.Email;
    this.role = employee.Role;
    this.contact = employee.Contact;
    this.salary = employee.salary;
    this.action = employee.Action;
  }
}
