import { Component, ViewChild } from '@angular/core';
import { Employee } from './template-driven.model';
import { NgForm } from '@angular/forms';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css'],
})
export class TemplateDrivenComponent {
  @ViewChild(MatTable) table: MatTable<any>;
  dataSource: any[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'address',
    'email',
    'role',
    'contact',
    'salary',
    'actions',
  ];
  employee: Employee;

  employeeList = {
    Name: '',
    Address: '',
    Email: '',
    Role: '',
    Contact: '',
    Salary: '',
    // Action: '',
  };

  constructor() {
    this.employee = new Employee(this.employeeList);
  }

  onSubmit(data: NgForm) {
    console.log(data.value);
  }

  isUpdate: boolean = false;
  updateId: number = -1;
  editRow(element: any) {
    console.log(element);
    this.isUpdate = true;
    this.updateId = this.dataSource.indexOf(element);
    // this.loginForm.setValue(element);
  }
  addEmployee(form: NgForm) {
    const newEmployee = this.employee;
    this.dataSource.push(newEmployee);
    this.table.renderRows();

    // this.toastr.success('Created new employee!');
    // console.log(this.employee.value);
  }
  deleteEmployee(element: any) {
    const deleteRow = this.dataSource.indexOf(element);
    this.dataSource.splice(deleteRow, 1);
    this.table.renderRows();
  }
}
