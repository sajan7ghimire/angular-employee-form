import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateDrivenRoutingModule } from './template-driven-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [],
  imports: [CommonModule, TemplateDrivenRoutingModule, SharedModule,  ToastrModule.forRoot(),],

})
export class TemplateDrivenModule {}
