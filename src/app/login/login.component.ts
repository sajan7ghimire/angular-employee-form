import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { json } from 'express';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  loginForm: FormGroup;
  constructor() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }
  onSubmit() {
    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
      localStorage.setItem('token', 'password');
      localStorage.setItem('userId', JSON.stringify(this.loginForm.value));
    }
  }


  get email() {
    return this.loginForm.get('email');
  }
}
