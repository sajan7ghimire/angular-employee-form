import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { enviroment } from '../enviroment/enviroment';

export class AuthIntercepter implements HttpInterceptor {
  private baseUrl = enviroment.baseUrl;
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log('Request is on the way');
    console.log('baseUrl', this.baseUrl);
    const modifiedRequest = req.clone({
      // modified the url add the path with the baseUrl
      url: `${this.baseUrl}/${req.url}`,
    });
    return next.handle(modifiedRequest);
  }
}
