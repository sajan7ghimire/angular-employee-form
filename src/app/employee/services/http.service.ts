import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  // private baseUrl = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {}

  getData(path: string): Observable<any> {
    return this.http.get(path);
  }
  getOneData(path: string): Observable<any> {
    return this.http.get(path);
  }

  postData(data: any, path: string): Observable<any> {
    return this.http.post(path, data);
  }
  putData(path: any, data: any): Observable<any> {
    {
      return this.http.put(path, data);
    }
  }
  deleteData(path: any) {
    return this.http.delete(path);
  }
}
