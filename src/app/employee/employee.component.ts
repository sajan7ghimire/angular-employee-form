import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { MatTable } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';

import { HttpService } from './services/http.service';

@Component({
  selector: 'app-emplyoee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  dataSource: any[] = [];

  @ViewChild(MatTable) table: MatTable<any>;
  isUpdate: boolean = false;
  updateId: number = -1;
  items: FormArray;
  employeeForm: FormGroup;
  // DATA PATHS

  private postPath = 'employee/newEmployee';
  private getPath = 'employee/allEmployee';
  private putPath = 'employee/update-employee';
  private deleteData = 'employee/delete-employee';
  displayedColumns: string[] = [
    'emp_id',
    'firstName',
    'lastName',
    'position',
    'salary',
    'address',
    'hobbies',
    'actions',
  ];

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private httpService: HttpService
  ) {
    this.employeeForm = this.fb.group({
      firstName: new FormControl(' ', Validators.required),

      lastName: new FormControl(' ', Validators.required),
      salary: new FormControl('', Validators.required),

      position: new FormControl(' ', Validators.required),
      address: new FormControl(' ', Validators.required),
      hobbies: this.fb.array([this.createHobby()]),
    });
  }
  ngOnInit() {
    this.httpService.getData(this.getPath).subscribe((data) => {
      this.dataSource = data.allEmployee;
      this.table.renderRows();
    });
  }
  get firstName(): FormControl {
    return this.employeeForm.get('firstName') as FormControl;
  }
  get lastName(): FormControl {
    return this.employeeForm.get('lastName') as FormControl;
  }
  get position(): FormControl {
    return this.employeeForm.get('position') as FormControl;
  }
  get salary(): FormControl {
    return this.employeeForm.get('salary') as FormControl;
  }
  get address(): FormControl {
    return this.employeeForm.get('address') as FormControl;
  }

  get hobbies(): FormArray {
    return this.employeeForm.controls['hobbies'] as FormArray;
  }

  createHobby(): FormGroup {
    return this.fb.group({
      hobby: ['', Validators.required],
    });
  }
  addHobby(): void {
    (<FormArray>this.employeeForm.get('hobbies')).push(this.createHobby());
  }

  removeItem(index: any) {
    (<FormArray>this.employeeForm.get('hobbies')).removeAt(index);
  }

  // ADDDING EMPLOYEE
  onAddEmployee() {
    const newEmployee = this.employeeForm.value;
    this.httpService.postData(newEmployee, this.postPath).subscribe(() => {
      this.httpService.getData(this.getPath).subscribe((data: any) => {
        this.dataSource = data.allEmployee;
        console.log('Getting for database', this.dataSource);
        this.table.renderRows();
        console.log('Posting value', this.employeeForm.value);
      });
    });
    this.isUpdate = false;

    this.employeeForm.reset();

    this.toastr.success('Created new employee!');
  }
  // EDITING ROW
  editRow(element: any) {
    this.isUpdate = true;
    const rowIndex = this.dataSource.indexOf(element);
    this.updateId = rowIndex;
    console.log('Editing elemet', element);
    this.employeeForm.patchValue({
      _id: element._id,
      firstName: element.firstName,
      lastName: element.lastName,
      salary: element.salary,
      address: element.address,
      position: element.position,
      hobbies: element.hobbies,
    });
    this.employeeForm.setControl(
      'hobbies',
      this.setExistingHobbies(element.hobbies)
    );
  }

  setExistingHobbies(hobbies: any[]): FormArray {
    const formArray: any = new FormArray([]);
    hobbies.forEach((hobby: any) => {
      formArray.push(
        this.fb.group({
          hobby: hobby.hobby,
        })
      );
    });
    return formArray;
  }
  //? UPDATING EMPLOYEE
  onUpdateEmployee() {
    const editData = this.employeeForm.value;
    const employeeId = this.dataSource[this.updateId]._id;
    this.httpService
      .putData(`${this.putPath}/${employeeId}`, editData)
      .subscribe(() => {
        this.dataSource[this.updateId] = editData;
        console.log('update id', this.updateId);
        this.table.renderRows();
        this.employeeForm.reset();
        this.isUpdate = false;
        this.updateId = -1;
      });
    this.toastr.info('Data Updated');
    console.log('Updating data', this.dataSource);
  }

  //? DELETIG EMPLOYEES
  deleteEmployee(element: any) {
    const deleteRow = this.dataSource.indexOf(element);
    console.log('Deleting data', element);
    if (confirm('Are you sure,you want to delete?'))
      this.httpService
        .deleteData(`${this.deleteData}/${element._id}`)
        .subscribe(() => {
          this.dataSource.splice(deleteRow, 1);
          console.log('Employee  deleting', deleteRow);
          this.table.renderRows();
        });
    this.toastr.warning('Employee Removed');
  }
}
